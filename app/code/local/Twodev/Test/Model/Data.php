<?php
/**
 * Twodev test data model
 *
 * @category    Twodev
 * @package     Twodev_Test
 * @author      Kim Vinqueur <kim.vinqueur@gmail.com>
 */

class Twodev_Test_Model_Data extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('twodev_test/data');
    }

    public function insertData()
    {
        $dummies = [
            [
                'value_1' => 'test2',
                'value_2' => 'test2',
                'value_3' => 'test2',
            ]
        ];

        foreach ($dummies as $dummy) {
            try {
                Mage::getModel('twodev_test/data')
                    ->setData($dummy)
                    ->save();
            } catch (Exception $exception) {
                echo $e->getMessage();
            }
        }
    }
}