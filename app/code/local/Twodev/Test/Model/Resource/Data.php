<?php
/**
 * Twodev test data resource model
 *
 * @category    Twodev
 * @package     Twodev_Test
 * @author      Kim Vinqueur <kim.vinqueur@gmail.com>
 */


class Twodev_Test_Model_Resource_Data extends Mage_Core_Model_Resource_Db_Abstract
{
    /**
     * Resource initialization
     */
    protected function _construct()
    {
        $this->_init('twodev_test/data', 'test_id');
    }
}