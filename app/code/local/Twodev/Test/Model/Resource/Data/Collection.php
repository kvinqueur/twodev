<?php

/**
 * Twodev test data collection
 *
 * @category    Twodev
 * @package     Twodev_Test
 * @author      Kim Vinqueur <kim.vinqueur@gmail.com>
 */
class Twodev_Test_Model_Resource_Data_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('twodev_test/data');
    }
}