<?php

/**
 * Twodev test controller
 *
 * @category    Twodev
 * @package     Twodev_Test
 * @author      Kim Vinqueur <kim.vinqueur@gmail.com>
 */

class Twodev_Test_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        echo Mage::getModel('twodev_test/data')->insertData();
        $this->loadLayout();
        $this->renderLayout();
    }
}