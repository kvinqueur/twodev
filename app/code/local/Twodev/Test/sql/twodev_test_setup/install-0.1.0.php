<?php
$installer = $this;
$installer->startSetup();
$table = $installer->getConnection()->newTable($installer->getTable('twodev_test/data'))
    ->addColumn('test_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned' => true,
        'nullable' => false,
        'primary' => true,
        'identity' => true,
    ), 'test ID')
    ->addColumn('value_1', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false,
    ), 'value 1')
    ->addColumn('value_2', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => true,
    ), 'value 2')
    ->addColumn('value_3', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(), 'value 3')
    ->setComment('twodev/test entity table');
$installer->getConnection()->createTable($table);
$installer->endSetup();